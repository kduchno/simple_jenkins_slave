FROM ubuntu:16.04

RUN apt-get update && apt upgrade -y
RUN apt-get install -y openssh-server net-tools iputils-ping openjdk-8-jdk \
    arp-scan git cmake ninja-build python3 python3-pip curl vim sudo

RUN pip3 install beautifulsoup4
RUN useradd -m -p jenkins -s /bin/bash jenkins
RUN usermod -a -G sudo jenkins
RUN sed -i 's|session required pam_loginuid.so|session optional pam_loginuid.so|g' /etc/pam.d/sshd
RUN mkdir -p /var/run/sshd
RUN echo "jenkins:jenkins" | chpasswd
RUN mkdir -p /home/jenkins/slaveshare
RUN mkdir -p /home/jenkins/.ssh

COPY ./sshkeys/authorized_keys /home/jenkins/.ssh/
COPY ./sshkeys/slave1 /home/jenkins/.ssh/
COPY ./sshkeys/slave1.pub /home/jenkins/.ssh/

RUN chown -R jenkins:jenkins /home/jenkins/slaveshare
RUN chown -R jenkins:jenkins /home/jenkins/.ssh

EXPOSE 22

CMD ["/usr/sbin/sshd", "-D"]
